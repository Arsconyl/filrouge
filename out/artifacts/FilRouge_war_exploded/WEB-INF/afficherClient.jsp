<%--
  Created by IntelliJ IDEA.
  User: costa
  Date: 05/07/2018
  Time: 18:21
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="fr">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../inc/head.jsp" />
<body>
<c:import url="../inc/header.jsp" />
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
                <fieldset>
                    <legend>Informations client</legend>
                    <p>Nom : <c:out value="${Client.nom}"/></p><br>
                    <p>Prénom : <c:out value="${Client.prenom}"/></p><br>
                    <p>Adresse : <c:out value="${Client.adresse}"/></p><br>
                    <p>Téléphone : <c:out value="${Client.tel}"/></p><br>
                    <p>E-mail : <c:out value="${Client.email}"/></p><br>
                </fieldset>
            <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
        </div>
    </div>
</div>
<c:import url="../inc/footer.jsp" />
</body>

</html>

