<%--
  Created by IntelliJ IDEA.
  User: costa
  Date: 05/07/2018
  Time: 18:22
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="fr">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../inc/head.jsp" />
<body>
<c:import url="../inc/header.jsp" />
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <form method="post" action="<c:url value="/CreationCommande" />" enctype="multipart/form-data" accept-charset="utf-8">
                <%--<c:import url="../inc/client_form.jsp" />--%>
                <fieldset>
                    <legend>Informations client</legend>
                    <label>Nouveau Client ?<span class="requis">*</span></label>
                    <input type="radio" id="choixNouveauClient" name="choixNouveauClient" value="nouveauClient" checked="checked" /> Oui
                    <input type="radio" id="choixNouveauClient" name="choixNouveauClient" value="ancienClient" <c:if test="${ empty sessionScope.listeClients }" var="NexisteClients"> disabled="disabled"</c:if> /> Non
                    <br />
                    <c:if test="${ !NexisteClients }">
                    <div class="clientEx" style="display: none;">
                        <select name="ClientSession" title="ClientSession">
                            <c:forEach var="listeClients" items="${listeClients}">
                                <option value="<c:out value="${listeClients.id}"/>"><c:out value="${listeClients.nom}"/> <c:out value="${listeClients.prenom}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                    </c:if>
                    <br />
                    <div class="clientNonEx" style="">
                        <c:import url="../inc/client_form.jsp" />
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Informations commande</legend>

                    <label for="dateCommande">Date <span class="requis">*</span></label>
                    <input type="text" id="dateCommande" name="dateCommande" value="<c:out value="${newCommande.date}" />" size="20" maxlength="20" disabled />
                    <span class="erreur">${formCommande.erreurs['dateCommande']}</span>
                    <br />

                    <label for="montantCommande">Montant <span class="requis">*</span></label>
                    <input type="text" id="montantCommande" name="montantCommande" value="<c:out value="${newCommande.montant}" />" size="20" maxlength="20" />
                    <span class="erreur">${formCommande.erreurs['montantCommande']}</span>
                    <br />

                    <label for="modePaiementCommande">Mode de paiement <span class="requis">*</span></label>
                    <input type="text" id="modePaiementCommande" name="modePaiementCommande" value="<c:out value="${newCommande.paiement}" />" size="20" maxlength="20" />
                    <span class="erreur">${formCommande.erreurs['modePaiementCommande']}</span>
                    <br />

                    <label for="statutPaiementCommande">Statut du paiement</label>
                    <input type="text" id="statutPaiementCommande" name="statutPaiementCommande" value="<c:out value="${newCommande.statutP}" />" size="20" maxlength="20" />
                    <span class="erreur">${formCommande.erreurs['statutPaiementCommande']}</span>
                    <br />

                    <label for="modeLivraisonCommande">Mode de livraison <span class="requis">*</span></label>
                    <input type="text" id="modeLivraisonCommande" name="modeLivraisonCommande" value="<c:out value="${newCommande.livraison}" />" size="20" maxlength="20" />
                    <span class="erreur">${formCommande.erreurs['modeLivraisonCommande']}</span>
                    <br />

                    <label for="statutLivraisonCommande">Statut de la livraison</label>
                    <input type="text" id="statutLivraisonCommande" name="statutLivraisonCommande" value="<c:out value="${newCommande.statutL}" />" size="20" maxlength="20" style="margin-bottom: 1%;" />
                    <span class="erreur">${formCommande.erreurs['statutLivraisonCommande']}</span>
                    <br />
                </fieldset>
                <input type="submit" value="Valider" class="btn btn-primary" />
                <input type="reset" value="Remettre à zéro" class="btn btn-primary" /> <br />

                <p class="${empty form.erreurs ? (empty formCommande.erreurs ? 'succes' : 'erreur') : 'erreur'}">${formCommande.resultat}</p>
            </form>
        </div>
    </div>
</div>
<c:import url="../inc/footer.jsp" />
</body>

</html>

