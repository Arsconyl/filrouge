<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="../inc/head.jsp" />
<body>
<c:import url="../inc/header.jsp" />
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
            <c:choose>
                <c:when test="${!empty listeCommandes}">
                        <table class="liste">
                            <tr>
                                <td>Client</td>
                                <td>Date</td>
                                <td>Montant</td>
                                <td>Mode de paiement</td>
                                <td>Statut de paiement</td>
                                <td>Mode de livraison</td>
                                <td>Statut de livraison</td>
                                <td class="action">Action</td>
                            </tr>
                            <c:forEach var="listeCommandes" items="${ listeCommandes }" varStatus="boucle">
                                <tr class="${boucle.index % 2 == 0 ? 'pair' : 'impair'}">
                                    <td><c:out value="${listeCommandes.client.nom}"/><br /><c:out value="${listeCommandes.client.prenom}"/></td>
                                    <%--<td><c:out value="${listeCommandes.date}"/></td>--%>
                                    <td><joda:format value="${ listeCommandes.date }" pattern="dd/MM/yyyy HH:mm:ss"/></td>
                                    <td><c:out value="${listeCommandes.montant}"/></td>
                                    <td><c:out value="${listeCommandes.paiement}"/></td>
                                    <td><c:out value="${listeCommandes.statutP}"/></td>
                                    <td><c:out value="${listeCommandes.livraison}"/></td>
                                    <td><c:out value="${listeCommandes.statutL}"/></td>
                                    <td><a href="<c:url value="/SuppressionCommande"><c:param name="id" value="${listeCommandes.id}" /></c:url>"><img src="<c:url value="./inc/supprimer.png" />"></a></td>
                                </tr>
                            </c:forEach>
                        </table>
                </c:when>
                <c:otherwise>
                    <p class="erreur">Aucune commande enregistrée</td>
                </c:otherwise>
            </c:choose>
        </div>
      </div>
    </div>
<c:import url="../inc/footer.jsp" />
  </body>

</html>
