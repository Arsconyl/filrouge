<!DOCTYPE html>
<html lang="en">

<c:import url="./inc/head.jsp" />
<body>
<c:import url="./inc/header.jsp" />
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="mt-5">TP Fil Rouge Java EE</h1>
          <p class="lead">Arnaud Couderc</p>
          <ul class="list-unstyled">
            <li>Bootstrap 4.1.1</li>
            <li>jQuery 3.3.1</li>
          </ul>
        </div>
      </div>
    </div>
<c:import url="./inc/footer.jsp" />
  </body>

</html>
