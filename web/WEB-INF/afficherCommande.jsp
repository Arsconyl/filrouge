<%--
  Created by IntelliJ IDEA.
  User: costa
  Date: 05/07/2018
  Time: 18:21
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="fr">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../inc/head.jsp" />
<body>
<c:import url="../inc/header.jsp" />
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
                <fieldset>
                    <legend>Informations client</legend>
                    <p>Nom : <c:out value="${newCommande.client.nom}"/></p><br>
                    <p>Prénom : <c:out value="${newCommande.client.prenom}"/></p><br>
                    <p>Adresse : <c:out value="${newCommande.client.adresse}"/></p><br>
                    <p>Téléphone : <c:out value="${newCommande.client.tel}"/></p><br>
                    <p>E-mail : <c:out value="${newCommande.client.email}"/></p><br>
                </fieldset>
                <fieldset>
                    <legend>Informations commande</legend>
                    <p>Date : <c:out value="${newCommande.date}"/></p><br>
                    <p>Montant : <c:out value="${newCommande.montant}"/></p><br>
                    <p>Mode de paiement : <c:out value="${newCommande.paiement}"/></p><br>
                    <p>Statut de paiement : <c:out value="${newCommande.statutP}"/></p><br>
                    <p>Livraison : <c:out value="${newCommande.livraison}"/></p><br>
                    <p>Statut de livraison : <c:out value="${newCommande.statutL}"/></p><br>
                </fieldset>
            <p class="${empty form.erreurs ? (empty formCommande.erreurs ? 'succes' : 'erreur') : 'erreur'}">${formCommande.resultat}</p>
        </div>
    </div>
</div>
<c:import url="../inc/footer.jsp" />
</body>

</html>

