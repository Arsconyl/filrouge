<%--
  Created by IntelliJ IDEA.
  User: costa
  Date: 05/07/2018
  Time: 18:21
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="fr">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../inc/head.jsp" />
<body>
<c:import url="../inc/header.jsp" />
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <form method="post" action="<c:url value="/CreationClient" />" enctype="multipart/form-data" accept-charset="utf-8">
                <fieldset>
                    <legend>Informations client</legend>
                    <c:import url="../inc/client_form.jsp" />
                </fieldset>
                <input type="submit" value="Valider" class="btn btn-primary" />
                <input type="reset" value="Remettre à zéro" class="btn btn-primary" /> <br />

                <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
            </form>
        </div>
    </div>
</div>
<c:import url="../inc/footer.jsp" />
</body>

</html>

