package com.couderc3u.filrouge.forms;

import com.couderc3u.filrouge.Exceptions.FormValidationException;
import com.couderc3u.filrouge.entities.Client;
import com.couderc3u.filrouge.dao.ClientDAO;
import eu.medsea.mimeutil.MimeUtil;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

public class ClientForm
{
    private boolean ok;

    private static final String _nom       = "nomClient";
    private static final String _prenom    = "prenomClient";
    private static final String _adresse   = "adresseClient";
    private static final String _tel       = "telephoneClient";
    private static final String _email     = "emailClient";

    public static final String CHEMIN      = "chemin";
    public static final String ATT_IMAGE = "image";
    private static final int    TAILLE_TAMPON     = 1024000;

    private String              resultat;
    private Map<String, String> erreurs = new HashMap<String, String>();
    private ClientDAO clientDAO;

    public ClientForm(ClientDAO clientDAO) {
        this.clientDAO = clientDAO;
    }

    public String getResultat()
    {
        return resultat;
    }

    public Map<String, String> getErreurs()
    {
        return erreurs;
    }

    private void setErreur(String champ, String message)
    {
        erreurs.put(champ, message);
    }

    public boolean OK()
    {
        return ok;
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getValeurChamp(HttpServletRequest request, String nomChamp)
    {
        String valeur = request.getParameter(nomChamp);
        if (valeur == null || valeur.trim().length() == 0)
        {
            return null;
        }
        else
        {
            return valeur.trim();
        }
    }

    public Client creationClient(HttpServletRequest request, String chemin)
    {
        String nom       = getValeurChamp(request, _nom);
        String prenom    = getValeurChamp(request, _prenom);
        String adresse   = getValeurChamp(request, _adresse);
        String telephone = getValeurChamp(request, _tel);
        String email     = getValeurChamp(request, _email);

        Client newClient = new Client();

        try
        {
            validationNom(nom);
        } catch (Exception e)
        {
            setErreur(_nom, e.getMessage());
        }
        newClient.setNom(nom);

        try
        {
            validationPrenom(prenom);
        } catch (Exception e)
        {
            setErreur(_prenom, e.getMessage());
        }
        newClient.setPrenom(prenom);

        try
        {
            validationAdresse(adresse);
        } catch (Exception e)
        {
            setErreur(_adresse, e.getMessage());
        }
        newClient.setAdresse(adresse);

        try
        {
            validationTel(telephone);
        } catch (Exception e)
        {
            setErreur(_tel, e.getMessage());
        }
        newClient.setTel(telephone);

        try
        {
            validationEmail(email);
        } catch (Exception e)
        {
            setErreur(_email, e.getMessage());
        }
        newClient.setEmail(email);

        if(chemin != null)
            try {
                enregistrerFichier(request, chemin, newClient);
            } catch (FormValidationException e){}

        if (erreurs.isEmpty())
        {
            clientDAO.creer(newClient);
            resultat = "Client créé avec succès";
            ok = true;
        }
        else
        {
            resultat = "Échec de la création du client.";
            ok = false;
        }

        return newClient;
    }

    private void validationEmail(String email) throws Exception
    {
        if (email != null)
        {
            if (!email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)"))
            {
                throw new FormValidationException("Merci de saisir une adresse mail valide.");
            }
        }
    }

    private void validationNom(String nom) throws Exception
    {
        if (nom != null)
        {
            if (nom.length() < 2)
            {
                throw new FormValidationException("Le nom d'utilisateur doit contenir au moins 2 caractères.");
            }
        }
        else
            throw new FormValidationException("Merci de saisir votre nom.");
    }

    private void validationPrenom(String prenom) throws Exception
    {
        if (prenom != null && prenom.length() < 2)
        {
            throw new FormValidationException("Le prénom d'utilisateur doit contenir au moins 2 caractères.");
        }
    }

    private void validationAdresse(String adresse) throws Exception
    {
        if (adresse != null)
        {
            if (adresse.length() < 10)
            {
                throw new FormValidationException("L'adresse doit contenir au moins 10 caractères.");
            }
        }
        else
            throw new FormValidationException("Merci de saisir une adresse");
    }

    private void validationTel(String tel) throws Exception
    {
        if (tel != null)
        {
            if(tel.length() < 4)
            {
                throw new FormValidationException("Le n° de téléphone doit contenir au moins 4 numéros.");
            }
            if(!tel.matches("^[0-9]{4,}"))
            {
                throw new FormValidationException("Le n° de téléphone doit contenir des numéros.");
            }
        }
        else
            throw new FormValidationException("Merci de saisir un n° de téléphone");
    }

    public void enregistrerFichier( HttpServletRequest request, String chemin, Client client ) throws FormValidationException{

        /*
         * Récupération du contenu du champ fichier du formulaire. Il faut ici
         * utiliser la méthode getPart(), comme nous l'avions fait dans notre
         * servlet auparavant.
         */
        String nomFichier = null;
        InputStream contenuFichier = null;
        try {
            Part part = request.getPart( ATT_IMAGE );
            /*
             * Il faut déterminer s'il s'agit bien d'un champ de type fichier :
             * on délègue cette opération à la méthode utilitaire
             * getNomFichier().
             */
            nomFichier = getNomFichier( part );

            /*
             * Si la méthode a renvoyé quelque chose, il s'agit donc d'un
             * champ de type fichier (input type="file").
             */
            if ( nomFichier != null && !nomFichier.isEmpty() ) {
                /*
                 * Antibug pour Internet Explorer, qui transmet pour une
                 * raison mystique le chemin du fichier local à la machine
                 * du client...
                 *
                 * Ex : C:/dossier/sous-dossier/fichier.ext
                 *
                 * On doit donc faire en sorte de ne sélectionner que le nom
                 * et l'extension du fichier, et de se débarrasser du
                 * superflu.
                 */
                nomFichier = nomFichier.substring( nomFichier.lastIndexOf( '/' ) + 1 )
                        .substring( nomFichier.lastIndexOf( '\\' ) + 1 );

                /* Récupération du contenu du fichier */
                contenuFichier = part.getInputStream();

                /* Extraction du type MIME du fichier depuis l'InputStream */
                MimeUtil.registerMimeDetector( "eu.medsea.mimeutil.detector.MagicMimeMimeDetector" );
                Collection<?> mimeTypes = MimeUtil.getMimeTypes( contenuFichier );

                /*
                 * Si le fichier est bien une image, alors son en-tête MIME
                 * commence par la chaîne "image"
                 */
                if ( mimeTypes.toString().startsWith( "image" ) ) {
                    /* Ecriture du fichier sur le disque */
                    ecrireFichier( contenuFichier, nomFichier, chemin );
                } else {
                    throw new FormValidationException( "Le fichier envoyé doit être une image." );
                }
            }
        } catch ( IllegalStateException e ) {
            /*
             * Exception retournée si la taille des données dépasse les limites
             * définies dans la section <multipart-config> de la déclaration de
             * notre servlet d'upload dans le fichier web.xml
             */
            e.printStackTrace();
            throw new FormValidationException( "Le fichier envoyé ne doit pas dépasser 1Mo." );
        } catch ( IOException e ) {
            /*
             * Exception retournée si une erreur au niveau des répertoires de
             * stockage survient (répertoire inexistant, droits d'accès
             * insuffisants, etc.)
             */
            e.printStackTrace();
            throw new FormValidationException( "Erreur de configuration du serveur." );
        } catch ( ServletException e ) {
            /*
             * Exception retournée si la requête n'est pas de type
             * multipart/form-data.
             */
            e.printStackTrace();
            throw new FormValidationException(
                    "Ce type de requête n'est pas supporté, merci d'utiliser le formulaire prévu pour envoyer votre fichier." );
        }

/*
        // Si aucune erreur n'est survenue jusqu'à présent
        if ( erreurs.isEmpty() ) {
            // Validation du champ fichier.
            try {
                validationFichier( nomFichier, contenuFichier );
            } catch ( Exception e ) {
                setErreur( ATT_IMAGE, e.getMessage() );
            }
            client.setImage( nomFichier );
        }

        // Si aucune erreur n'est survenue jusqu'à présent
        if ( erreurs.isEmpty() ) {
            // Écriture du fichier sur le disque
            try {
                ecrireFichier( contenuFichier, nomFichier, chemin );
            } catch ( Exception e ) {
                setErreur( ATT_IMAGE, "Erreur lors de l'écriture du fichier sur le disque." );
            }
        }

        // Initialisation du résultat global de la validation.
        if ( erreurs.isEmpty() ) {
            resultat = "Succès de l'envoi du fichier.";
        } else {
            resultat = "Échec de l'envoi du fichier.";
        }
        */
    }

    private void validationFichier( String nomFichier, InputStream contenuFichier ) throws Exception {
        if ( nomFichier == null || contenuFichier == null ) {
            throw new FormValidationException( "Merci de sélectionner un fichier à envoyer." );
        }
        /* Extraction du type MIME du fichier depuis l'InputStream nommé "contenu" */
        MimeUtil.registerMimeDetector( "eu.medsea.mimeutil.detector.MagicMimeMimeDetector" );
        Collection<?> mimeTypes = MimeUtil.getMimeTypes( contenuFichier );

        /*
         * Si le fichier est bien une image, alors son en-tête MIME
         * commence par la chaîne "image"
         */
        if ( !mimeTypes.toString().startsWith( "image" ) )
            throw new FormValidationException ("Merci de sélectionner une image.");
    }

    /*
     * Méthode utilitaire qui a pour unique but d'analyser l'en-tête
     * "content-disposition", et de vérifier si le paramètre "filename" y est
     * présent. Si oui, alors le champ traité est de type File et la méthode
     * retourne son nom, sinon il s'agit d'un champ de formulaire classique et
     * la méthode retourne null.
     */
    private static String getNomFichier( Part part ) {
        /* Boucle sur chacun des paramètres de l'en-tête "content-disposition". */
        for ( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) {
            /* Recherche de l'éventuelle présence du paramètre "filename". */
            if ( contentDisposition.trim().startsWith( "filename" ) ) {
                /*
                 * Si "filename" est présent, alors renvoi de sa valeur,
                 * c'est-à-dire du nom de fichier sans guillemets.
                 */
                return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
            }
        }
        /* Et pour terminer, si rien n'a été trouvé... */
        return null;
    }

    /*
     * Méthode utilitaire qui a pour but d'écrire le fichier passé en paramètre
     * sur le disque, dans le répertoire donné et avec le nom donné.
     */
    /*
     * Méthode utilitaire qui a pour but d'écrire le fichier passé en paramètre
     * sur le disque, dans le répertoire donné et avec le nom donné.
     */
    private void ecrireFichier( InputStream contenuFichier, String nomFichier, String chemin )
            throws FormValidationException {
        /* Prépare les flux. */
        BufferedInputStream entree = null;
        BufferedOutputStream sortie = null;
        try {
            /* Ouvre les flux. */
            entree = new BufferedInputStream( contenuFichier, TAILLE_TAMPON );
            sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + nomFichier ) ),
                    TAILLE_TAMPON );

            /*
             * Lit le fichier reçu et écrit son contenu dans un fichier sur le
             * disque.
             */
            byte[] tampon = new byte[TAILLE_TAMPON];
            int longueur = 0;
            while ( ( longueur = entree.read( tampon ) ) > 0 ) {
                sortie.write( tampon, 0, longueur );
            }
        } catch ( Exception e ) {
            throw new FormValidationException( "Erreur lors de l'écriture du fichier sur le disque." );
        } finally {
            try {
                sortie.close();
            } catch ( IOException ignore ) {
            }
            try {
                entree.close();
            } catch ( IOException ignore ) {
            }
        }
    }
}
