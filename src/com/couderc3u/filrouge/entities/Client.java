package com.couderc3u.filrouge.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

@Entity
public class Client implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;
    private String nom, prenom, adresse, email, image;
    @Column ( name = "TELEPHONE")
    private String tel;

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        if(nom != null)
            this.nom = new String (nom.getBytes(), StandardCharsets.UTF_8);
    }

    public String getPrenom()
    {
        return prenom;
    }

    public void setPrenom(String prenom)
    {
        if(prenom != null)
            this.prenom = new String (prenom.getBytes(), StandardCharsets.UTF_8);
    }

    public String getAdresse()
    {
        return adresse;
    }

    public void setAdresse(String adresse)
    {
        if(adresse != null)
            this.adresse = new String (adresse.getBytes(), StandardCharsets.UTF_8);
    }

    public String getTel()
    {
        return tel;
    }

    public void setTel(String tel)
    {
        if(tel != null)
            this.tel = new String (tel.getBytes(), StandardCharsets.UTF_8);
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        if(email != null)
            this.email = new String (email.getBytes(), StandardCharsets.UTF_8);
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        if(image != null)
            this.image = new String (image.getBytes(), StandardCharsets.UTF_8);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
