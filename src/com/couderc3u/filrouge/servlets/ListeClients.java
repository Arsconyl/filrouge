package com.couderc3u.filrouge.servlets;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet( urlPatterns = { "/ListeClients" } )
public class ListeClients extends HttpServlet
{
    private static final String VUE = "/WEB-INF/listerClients.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        this.getServletContext().getRequestDispatcher( VUE ).forward( req, resp );
    }
}

