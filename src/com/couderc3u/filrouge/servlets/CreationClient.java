package com.couderc3u.filrouge.servlets;

import com.couderc3u.filrouge.entities.Client;
import com.couderc3u.filrouge.dao.ClientDAO;
import com.couderc3u.filrouge.forms.ClientForm;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet( urlPatterns = { "/CreationClient" }, initParams = @WebInitParam( name = "chemin", value = "/fichiers/images/" ) )
@MultipartConfig( location = "/fichiers", maxFileSize = 10 * 1024 * 1024, maxRequestSize = 5 * 10 * 1024 * 1024, fileSizeThreshold = 1024 * 1024 )
public class CreationClient extends HttpServlet
{

    private static final String ATT_CLIENT = "Client";
    private static final String ATT_FORM = "form";
    private static final String VUE = "/WEB-INF/listerClients.jsp";
    private static final String FORM = "/WEB-INF/creerClient.jsp";
    private static final String ATT_LISTE = "listeClients";

    private static final String CHAMP_IMAGE     = "image";
    public static final String CHEMIN      = "chemin";
    public static final String ATT_IMAGE = "image";

    public static final String CONF_DAO_FACTORY = "DAOfactory";

    @EJB
    private ClientDAO clientDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        this.getServletContext().getRequestDispatcher( FORM ).forward( req, resp );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        ClientForm  form      = new ClientForm(clientDAO);
        String chemin = this.getServletConfig().getInitParameter( CHEMIN );
        Client      newClient = form.creationClient(req, chemin);
        HttpSession session   = req.getSession();

        //req.setAttribute(ATT_CLIENT, newClient);
        req.setAttribute(ATT_FORM, form);
        if(form.getErreurs().isEmpty())
        {
            ajoutClientSession(session, newClient);
            this.getServletContext().getRequestDispatcher( VUE ).forward( req, resp );
        }
        else
            this.getServletContext().getRequestDispatcher( FORM ).forward( req, resp );
    }

    public static void ajoutClientSession(HttpSession session, Client newClient)
    {
        List<Client> listeClients = (List<Client>)session.getAttribute(ATT_LISTE);
        if(listeClients == null)
        {
            listeClients = new ArrayList<>();
            listeClients.add(newClient);
            session.setAttribute(ATT_LISTE, listeClients);
        }
        else
        {
            listeClients.add(newClient);
            session.setAttribute(ATT_LISTE, listeClients);
        }
    }
}
