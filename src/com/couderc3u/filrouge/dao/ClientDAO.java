package com.couderc3u.filrouge.dao;

import com.couderc3u.filrouge.Exceptions.DAOException;
import com.couderc3u.filrouge.entities.Client;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.sql.*;
import java.util.List;

@Stateless
public class ClientDAO{

    @PersistenceContext( unitName = "javaee_PU" )
    private EntityManager em;

    public Client trouver( long id ) throws DAOException {
        try {
            return em.find( Client.class, id );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    public void creer( Client client ) throws DAOException {
        try {
            em.persist( client );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    public List<Client> lister() throws DAOException {
        try {
            TypedQuery<Client> query = em.createQuery( "SELECT c FROM Client c ORDER BY c.id", Client.class );
            return query.getResultList();
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    public void supprimer( Client client ) throws DAOException {
        try {
            em.remove( em.merge( client ) );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }
}
